prepare:
	docker build -f docker/source.dockerfile -t source .

build: prepare
	docker-compose build

run:
	docker-compose up

stop:
	docker-compose down

clear:
	docker-compose rm
	docker rmi source

lint:
	golangci-lint run

test:
	go test
