package main

import (
    "currencies-task/config"
    "currencies-task/rest/generator"
    "currencies-task/rpc/history"
    "currencies-task/service/analyzer"
    "fmt"
    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials/insecure"
    "log"
    "time"
)

func main() {
    cfg := config.Parse()

    generatorClient := generator.NewClient(fmt.Sprintf("http://%s:%d", cfg.Generator.Host, cfg.Generator.Port))

    conn, err := grpc.Dial(
        fmt.Sprintf("%s:%d", cfg.History.Host, cfg.History.Port),
        grpc.WithTransportCredentials(insecure.NewCredentials()),
    )
    for err != nil {
        log.Println(err)
        time.Sleep(time.Second)
        conn, err = grpc.Dial(
            fmt.Sprintf("%s:%d", cfg.History.Host, cfg.History.Port),
            grpc.WithTransportCredentials(insecure.NewCredentials()),
        )
    }
    historyClient := history.NewHistoryClient(conn)

    service, err := analyzer.NewService(historyClient, generatorClient, cfg.Analyzer)
    if err != nil {
        log.Fatalln(err)
    }

    for range time.Tick(time.Second) {
        ohlcs, _ := service.OHLCs("USDRUB", time.Minute)
        log.Println(ohlcs)
    }
}
