package main

import (
    "currencies-task/config"
    "currencies-task/handler"
    "currencies-task/service/generator"
    "fmt"
    "github.com/gorilla/mux"
    "log"
    "math/rand"
    "net/http"
    "time"
)

func main() {
    cfg := config.Parse()

    rand.Seed(time.Now().UnixNano())
    b := generator.NewService(cfg.Generator)

    router := mux.NewRouter()
    router.Handle("/prices/{pair}", handler.NewPrices(b)).Methods(http.MethodGet)

    log.Printf("listening at http://localhost:%d\n", cfg.Generator.Port)
    log.Fatalln(http.ListenAndServe(fmt.Sprintf(":%d", cfg.Generator.Port), router))
}
