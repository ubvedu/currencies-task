package main

import (
    "currencies-task/config"
    "currencies-task/database/postgres"
    "currencies-task/rest/generator"
    history2 "currencies-task/rpc/history"
    "currencies-task/service/history"
    "fmt"
    "google.golang.org/grpc"
    "log"
    "net"
    "time"
)

func main() {
    cfg := config.Parse()

    client := generator.NewClient(fmt.Sprintf("http://%s:%d", cfg.Generator.Host, cfg.Generator.Port))

    database, err := postgres.NewDatabase(cfg.Postgres)
    for err != nil {
        log.Println(err)
        time.Sleep(time.Second)
        database, err = postgres.NewDatabase(cfg.Postgres)
    }

    service, err := history.NewService(client, database, cfg.History)
    if err != nil {
        log.Fatalln(err)
    }

    listener, err := net.Listen("tcp", fmt.Sprintf(":%d", cfg.History.Port))
    if err != nil {
        log.Fatalln(err)
    }

    server := grpc.NewServer()
    history2.RegisterHistoryServer(server, history2.NewServer(service))
    log.Fatalln(server.Serve(listener))
}
