package config

import (
    postgres "currencies-task/database/postgres"
    "currencies-task/service/analyzer"
    "currencies-task/service/generator"
    "currencies-task/service/history"
    "github.com/vrischmann/envconfig"
    "log"
)

type (
    Config struct {
        Generator generator.Config
        Postgres  postgres.Config
        History   history.Config
        Analyzer  analyzer.Config
    }
)

func Parse() Config {
    var conf Config
    if err := envconfig.Init(&conf); err != nil {
        log.Fatalln(err)
    }
    return conf
}
