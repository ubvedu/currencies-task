package postgres

import (
    "fmt"
    "gorm.io/driver/postgres"
    "gorm.io/gorm"
)

type (
    Database struct {
        db *gorm.DB
    }

    Config struct {
        User     string
        Password string
        Db       string
        Host     string
        Port     string
    }
)

func NewDatabase(config Config) (*Database, error) {
    db, err := gorm.Open(postgres.Open(config.Dsn()))
    if err != nil {
        return nil, err
    }

    if err = db.AutoMigrate(&Pair{}, &Price{}); err != nil {
        return nil, err
    }

    if err = db.Create([]Pair{
        {Name: "EURUSD"},
        {Name: "USDRUB"},
    }).Error; err != nil {
        return nil, err
    }

    return &Database{db: db}, nil
}

func (c Config) Dsn() string {
    return fmt.Sprintf(
        "host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
        c.Host, c.User, c.Password, c.Db, c.Port,
    )
}
