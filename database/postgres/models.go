package postgres

import (
    "github.com/google/uuid"
    "time"
)

type Pair struct {
    Id   int `gorm:"primaryKey"`
    Name string
}

type Price struct {
    Uuid       uuid.UUID `gorm:"primaryKey"`
    Value      float64   `gorm:"notNull"`
    UploadTime time.Time `gorm:"notNull"`
    PairId     int       `gorm:"notNull"`
    Pair       Pair
}
