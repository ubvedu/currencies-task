package postgres

import (
    "github.com/google/uuid"
    "gorm.io/gorm/clause"
    "time"
)

func (d *Database) Pairs() ([]Pair, error) {
    var pairs []Pair
    result := d.db.Find(&pairs)
    return pairs, result.Error
}

func (d *Database) SavePrices(pair string, prices []Price) error {
    pairId, err := d.PairId(pair)
    if err != nil {
        return err
    }

    for _, price := range prices {
        price.Uuid = uuid.New()
        price.PairId = pairId
        if err := d.db.Clauses(clause.OnConflict{
            DoNothing: true,
        }).Create(&price).Error; err != nil {
            return err
        }
    }
    return nil
}

func (d *Database) PairId(pairName string) (int, error) {
    var pair Pair
    result := d.db.Select("id").Take(&pair, "name = ?", pairName)
    return pair.Id, result.Error
}

func (d *Database) PricesFromUntil(pair string, from time.Time, until time.Time) ([]Price, error) {
    pairId, err := d.PairId(pair)
    if err != nil {
        return nil, err
    }

    var prices []Price
    result := d.db.Where(
        "pair_id = ? AND upload_time >= ? AND upload_time <= ?",
        pairId, from, until,
    ).Order("upload_time").Find(&prices)
    return prices, result.Error
}
