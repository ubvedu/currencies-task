FROM source AS source
RUN go build -o /main ./cmd/analyzer

FROM alpine
COPY --from=source /main .
ENTRYPOINT ["/main"]