FROM source AS source
RUN go build -o /main ./cmd/generator

FROM alpine
COPY --from=source /main .
ENTRYPOINT ["/main"]