FROM source AS source
RUN go build -o /main ./cmd/history

FROM alpine
COPY --from=source /main .
ENTRYPOINT ["/main"]