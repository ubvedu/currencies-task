FROM golang:alpine AS source
WORKDIR /src
COPY ./go.mod .
RUN go mod download
COPY . .