package domain

type OHLC struct {
    Open  float64
    High  float64
    Low   float64
    Close float64
}

func OHLCFrom(value float64) OHLC {
    return OHLC{
        Open:  value,
        High:  value,
        Low:   value,
        Close: value,
    }
}
