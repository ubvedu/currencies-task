package domain

import (
    "fmt"
)

type Pattern struct {
    Init      float64
    Trend     float64
    Roughness float64
}

func (p *Pattern) Decode(value string) error {
    _, err := fmt.Sscanf(value, "%f^%f~%f", &p.Init, &p.Trend, &p.Roughness)
    if err != nil {
        return fmt.Errorf("error matching format `init^trend~roughness` for `%s`", value)
    }
    return nil
}

func (p Pattern) Valid() error {
    switch {
    case p.Init <= 0:
        return fmt.Errorf("init value must be positive, but got %g", p.Init)
    case p.Roughness < 0:
        return fmt.Errorf("roughness must not be negative, but got %g", p.Roughness)
    }
    return nil
}
