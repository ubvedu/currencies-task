package domain

import (
    "fmt"
    "github.com/stretchr/testify/assert"
    "testing"
)

type test struct {
    raw      string
    expected Pattern
}

func TestPattern_Decode(t *testing.T) {
    for i, x := range []test{
        {raw: "80^10~0.02", expected: Pattern{Init: 80, Trend: 10, Roughness: 0.02}},
        {raw: "1.5^0.1~0.01", expected: Pattern{Init: 1.5, Trend: -0.1, Roughness: 0.01}},
        {raw: "100^5~0.01", expected: Pattern{Init: 100, Trend: -5, Roughness: 0.01}},
    } {
        t.Run(fmt.Sprint(i), func(t *testing.T) {
            var pattern Pattern
            err := pattern.Decode(x.raw)
            assert.Nil(t, err)
            assert.Equal(t, x.expected, pattern)
        })
    }
}

func TestPattern_Valid(t *testing.T) {
    assert.Nil(t, Pattern{Init: 80, Trend: 10, Roughness: 0.02}.Valid())
    assert.NotNil(t, Pattern{Init: -20, Trend: 0.5, Roughness: 0.05}.Valid())
    assert.NotNil(t, Pattern{Init: 10, Trend: -30, Roughness: -0.03}.Valid())
}
