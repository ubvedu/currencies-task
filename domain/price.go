package domain

import "time"

type Price struct {
    Value      float64   `json:"value"`
    UploadTime time.Time `json:"uploadTime"`
}
