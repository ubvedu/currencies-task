package domain

type PricesResponse struct {
    Prices []Price `json:"prices"`
}
