package handler

import (
    "currencies-task/domain"
    "currencies-task/service/generator"
    "encoding/json"
    "fmt"
    "github.com/gorilla/mux"
    "log"
    "net/http"
)

type Prices struct {
    service *generator.Service
}

func NewPrices(service *generator.Service) *Prices {
    return &Prices{
        service: service,
    }
}

func (p *Prices) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    pair := domain.Pair(mux.Vars(r)["pair"])
    prices := make([]domain.Price, p.service.Size())
    ok, n := p.service.ReadAll(pair, prices)
    if !ok {
        http.Error(w, fmt.Sprintf("pair not found: %s", pair), http.StatusNotFound)
        return
    }
    prices = prices[:n]
    w.Header().Set("Content-Type", "application/json")
    if err := json.NewEncoder(w).Encode(domain.PricesResponse{Prices: prices}); err != nil {
        log.Println(err)
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
}
