package generator

import (
    "currencies-task/domain"
    "encoding/json"
    "fmt"
    "net/http"
)

type Client struct {
    url string
}

func NewClient(url string) *Client {
    return &Client{
        url: url,
    }
}

func (c *Client) GetPrices(pair domain.Pair) ([]domain.Price, error) {
    resp, err := http.Get(fmt.Sprintf("%s/prices/%s", c.url, pair))
    if err != nil {
        return nil, err
    }

    var prices domain.PricesResponse
    if err = json.NewDecoder(resp.Body).Decode(&prices); err != nil {
        return nil, err
    }

    return prices.Prices, nil
}
