package history

import (
    "currencies-task/domain"
    "currencies-task/service/history"
    "google.golang.org/protobuf/types/known/timestamppb"
)

type Server struct {
    UnimplementedHistoryServer
    service *history.Service
}

func NewServer(service *history.Service) *Server {
    return &Server{
        service: service,
    }
}

func (s *Server) Prices(r *PricesRequest, stream History_PricesServer) error {
    prices, err := s.service.PricesFromUntil(domain.Pair(r.GetPair()), r.GetFrom().AsTime(), r.GetUntil().AsTime())
    if err != nil {
        return err
    }

    for _, price := range prices {
        if err = stream.Send(&Price{
            Value:      price.Value,
            UploadTime: timestamppb.New(price.UploadTime),
        }); err != nil {
            return err
        }
    }

    return nil
}
