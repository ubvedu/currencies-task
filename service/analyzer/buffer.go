package analyzer

import (
    "currencies-task/domain"
    "math"
    "time"
)

type buffer struct {
    initialized bool
    ohlcs       []domain.OHLC
    start       time.Time
    now         time.Time
    period      time.Duration
}

func newBuffer(period time.Duration) *buffer {
    return &buffer{
        ohlcs:  []domain.OHLC{},
        period: period,
    }
}

func (b *buffer) init(price domain.Price) {
    t := price.UploadTime.Truncate(b.period)
    b.start = t
    b.now = t
    b.ohlcs = append(b.ohlcs, domain.OHLCFrom(price.Value))
    b.initialized = true
}

func (b *buffer) parse(prices []domain.Price) {
    if !b.initialized && len(prices) != 0 {
        b.init(prices[0])
    }
    ohlc := &b.ohlcs[len(b.ohlcs)-1]
    for _, price := range prices {
        if price.UploadTime.Before(b.now) {
            continue
        }
        for price.UploadTime.After(b.now.Add(b.period)) {
            b.ohlcs = append(b.ohlcs, domain.OHLCFrom(ohlc.Close))
            ohlc = &b.ohlcs[len(b.ohlcs)-1]
            b.now = b.now.Add(b.period)
        }
        ohlc.High = math.Max(ohlc.High, price.Value)
        ohlc.Low = math.Min(ohlc.Low, price.Value)
        ohlc.Close = price.Value
    }
}
