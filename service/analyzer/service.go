package analyzer

import (
    "context"
    "currencies-task/domain"
    "currencies-task/rest/generator"
    "currencies-task/rpc/history"
    "google.golang.org/protobuf/types/known/timestamppb"
    "io"
    "log"
    "sync"
    "time"
)

type (
    Service struct {
        mutex sync.RWMutex
        ohlcs map[domain.Pair]map[time.Duration]*buffer

        updatePeriod  time.Duration
        accessHistory bool

        generator *generator.Client
        history   history.HistoryClient
    }

    Config struct {
        Timeframes    []time.Duration
        Pairs         []string
        UpdatePeriod  time.Duration
        AccessHistory bool
    }
)

func NewService(h history.HistoryClient, g *generator.Client, config Config) (*Service, error) {

    s := &Service{
        ohlcs:         make(map[domain.Pair]map[time.Duration]*buffer),
        updatePeriod:  config.UpdatePeriod,
        accessHistory: config.AccessHistory,
        generator:     g,
        history:       h,
    }

    for _, pair := range config.Pairs {
        m := make(map[time.Duration]*buffer)
        for _, timeframe := range config.Timeframes {
            m[timeframe] = newBuffer(timeframe)
        }
        s.ohlcs[domain.Pair(pair)] = m
    }

    go s.run()

    return s, nil
}

func (s *Service) AvailablePeriods(pair domain.Pair) []time.Duration {
    s.mutex.RLock()
    periods := make([]time.Duration, 0, len(s.ohlcs))
    for period := range s.ohlcs[pair] {
        periods = append(periods, period)
    }
    s.mutex.RUnlock()
    return periods
}

func (s *Service) OHLCs(pair domain.Pair, period time.Duration) (ohlcs []domain.OHLC, ok bool) {
    s.mutex.RLock()
    pairOHLCs, ok := s.ohlcs[pair]
    if !ok {
        return
    }
    src, ok := pairOHLCs[period]
    if !ok {
        return
    }
    ohlcs = make([]domain.OHLC, len(src.ohlcs))
    copy(ohlcs, src.ohlcs)
    s.mutex.RUnlock()
    return
}

func (s *Service) run() {

    now := time.Now()

    if s.accessHistory {
        s.mutex.Lock()
        for pair, buffers := range s.ohlcs {
            prices, err := getPrices(s.history, pair, now)
            for err != nil {
                log.Println(err)
                time.Sleep(time.Second)
                prices, err = getPrices(s.history, pair, now)
            }
            parsePrices(buffers, prices)
        }
        s.mutex.Unlock()
    }

    for range time.Tick(s.updatePeriod) {
        s.mutex.Lock()
        for pair, periodOhlcs := range s.ohlcs {
            prices, err := s.generator.GetPrices(pair)
            if err != nil {
                log.Fatalln(err)
            }

            parsePrices(periodOhlcs, prices)
        }
        s.mutex.Unlock()
    }
}

func getPrices(hc history.HistoryClient, pair domain.Pair, now time.Time) ([]domain.Price, error) {
    stream, err := hc.Prices(context.Background(), &history.PricesRequest{
        Pair:  string(pair),
        From:  timestamppb.New(now.Truncate(24 * time.Hour)),
        Until: timestamppb.New(now),
    })
    if err != nil {
        return nil, err
    }

    var (
        prices []domain.Price
        price  *history.Price
    )
    price, err = stream.Recv()
    for err != io.EOF {
        prices = append(prices, domain.Price{
            Value:      price.GetValue(),
            UploadTime: price.GetUploadTime().AsTime(),
        })
        price, err = stream.Recv()
    }
    log.Println("received from history:", len(prices))
    return prices, nil
}

func parsePrices(buffers map[time.Duration]*buffer, prices []domain.Price) {
    for _, buf := range buffers {
        buf.parse(prices)
    }
}
