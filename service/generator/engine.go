package generator

import (
    "currencies-task/domain"
    "math"
    "math/rand"
)

type Engine struct {
    pattern domain.Pattern
    buffer  []float64
    nextIdx int

    process chan struct{}
}

func NewEngine(minBufferSize int, pattern domain.Pattern) *Engine {
    bufferSize := nextPowerOf2(minBufferSize-1) + 1

    if err := pattern.Valid(); err != nil {
        panic(err)
    }

    g := &Engine{
        pattern: pattern,
        buffer:  make([]float64, bufferSize),
        nextIdx: 0,
        process: make(chan struct{}, 1),
    }

    go g.initValues()

    return g
}

func (g *Engine) Next() (value float64) {
    if g.nextIdx == 0 {
        <-g.process
    }
    value = math.Exp(g.buffer[g.nextIdx])
    g.nextIdx++
    if g.nextIdx == len(g.buffer)-1 {
        g.process = make(chan struct{}, 1)
        go g.updateValues()
        g.nextIdx = 0
    }
    return
}

func (g *Engine) initValues() {
    chunkSize := len(g.buffer) - 1
    initLog := math.Log(g.pattern.Init)
    fill(
        g.buffer,
        g.pattern.Roughness,
        initLog,
        initLog+float64(chunkSize)*g.pattern.Trend+midpointBias(g.pattern.Roughness, 2*chunkSize),
    )
    close(g.process)
}

func (g *Engine) updateValues() {
    chunkSize := len(g.buffer) - 1
    first := g.buffer[0]
    middle := g.buffer[chunkSize]
    fill(
        g.buffer,
        g.pattern.Roughness,
        middle,
        2*middle-first-midpointBias(g.pattern.Roughness, 2*chunkSize),
    )
    close(g.process)
}

func fill(values []float64, roughness float64, first float64, last float64) {
    values[0] = first
    values[len(values)-1] = last
    fillRecursive(values, roughness)
}

func fillRecursive(values []float64, roughness float64) {
    if len(values) <= 2 {
        return
    }
    middle := (len(values) - 1) / 2
    values[middle] = 0.5*(values[0]+values[len(values)-1]) + midpointBias(roughness, len(values)-1)
    fillRecursive(values[:middle+1], roughness)
    fillRecursive(values[middle:], roughness)
}

func midpointBias(roughness float64, width int) float64 {
    return roughness * float64(width) * randomSigned()
}

func nextPowerOf2(x int) int {
    y := 1
    for x > 0 {
        y <<= 1
        x >>= 1
    }
    return y
}

func randomSigned() float64 {
    return 2*rand.Float64() - 1
}
