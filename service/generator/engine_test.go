package generator

import (
    "currencies-task/domain"
    "fmt"
    "github.com/stretchr/testify/assert"
    "math"
    "math/rand"
    "testing"
)

type param struct {
    minBufferSize int
    pattern       domain.Pattern
}

var params = []param{
    {minBufferSize: 5, pattern: domain.Pattern{Init: 50, Trend: 0.2, Roughness: 0.1}},
    {minBufferSize: 3, pattern: domain.Pattern{Init: 1, Trend: -0.2, Roughness: 0.2}},
    {minBufferSize: 4, pattern: domain.Pattern{Init: 40, Trend: 0.5, Roughness: 0.5}},
}

func newEngines() []*Engine {
    generators := make([]*Engine, 0, len(params))
    for _, p := range params {
        generators = append(generators, NewEngine(p.minBufferSize, p.pattern))
    }
    return generators
}

func TestNew(t *testing.T) {
    rand.Seed(1660126574140496395)
    gs := newEngines()
    for i, g := range gs {
        <-g.process
        t.Run(fmt.Sprint(i), func(t *testing.T) {
            assert.Equal(t, 0, g.nextIdx)
            assert.Equal(t, params[i].pattern, g.pattern)
            assert.GreaterOrEqual(t, len(g.buffer), params[i].minBufferSize)
            t.Log(g.buffer)
        })
    }
}

func TestEngine_Next(t *testing.T) {
    rand.Seed(1660126574140496395)
    gs := newEngines()
    for i, g := range gs {
        t.Run(fmt.Sprint(i), func(t *testing.T) {
            for j := 0; j < 10; j++ {
                value := g.Next()
                assert.NotEmpty(t, value)
                assert.Equal(t, math.Exp(g.buffer[j%(len(g.buffer)-1)]), value)
                assert.Less(t, 0., value)
            }
        })
    }
}

func TestNextPowerOf2(t *testing.T) {
    assert.Equal(t, 128, nextPowerOf2(72))
    assert.Equal(t, 32, nextPowerOf2(25))
    assert.Equal(t, 256, nextPowerOf2(133))
    assert.Equal(t, 64, nextPowerOf2(32))
}
