package generator

import (
    "currencies-task/domain"
    "strings"
    "sync"
    "time"
)

type (
    Service struct {
        size         int
        pairs        map[domain.Pair]*pairData
        tickDuration time.Duration

        mutex sync.RWMutex
    }

    Config struct {
        Host         string
        Port         int
        Pairs        PatternMap
        BufferSize   int
        UpdatePeriod time.Duration
    }

    pairData struct {
        prices []domain.Price
        engine *Engine
    }
)

func NewService(config Config) *Service {
    pairs := make(map[domain.Pair]*pairData)
    for pair, pattern := range config.Pairs {
        pairs[pair] = NewPair(config.BufferSize, pattern)
    }

    s := &Service{
        pairs:        pairs,
        size:         config.BufferSize,
        tickDuration: config.UpdatePeriod,
    }

    go s.run()

    return s
}

func (s *Service) Size() int {
    return s.size
}

func (s *Service) ReadAll(pair domain.Pair, prices []domain.Price) (ok bool, n int) {
    s.mutex.RLock()

    data, ok := s.pairs[pair]
    if !ok {
        return
    }

    n = copy(prices, data.prices)

    s.mutex.RUnlock()
    return
}

func (s *Service) run() {
    for currentTime := range time.Tick(s.tickDuration) {
        s.mutex.Lock()
        for _, pair := range s.pairs {
            pair.update(s.size, currentTime)
        }
        s.mutex.Unlock()
    }
}

func NewPair(bufferSize int, pattern domain.Pattern) *pairData {
    return &pairData{
        prices: make([]domain.Price, 0, bufferSize),
        engine: NewEngine(bufferSize, pattern),
    }
}

func (p *pairData) update(bufferSize int, currentTime time.Time) {
    p.prices = append(p.prices, domain.Price{
        Value:      p.engine.Next(),
        UploadTime: currentTime,
    })
    if len(p.prices) > bufferSize {
        p.prices = p.prices[len(p.prices)-bufferSize:]
    }
}

type PatternMap map[domain.Pair]domain.Pattern

func (m *PatternMap) Unmarshal(s string) error {
    *m = make(PatternMap)
    entries := strings.Split(s, ",")
    for _, entry := range entries {
        split := strings.Split(entry, ":")
        var pattern domain.Pattern
        if err := pattern.Decode(split[1]); err != nil {
            return err
        }
        (*m)[domain.Pair(split[0])] = pattern
    }
    return nil
}
