package generator

import (
    "currencies-task/config"
    "currencies-task/domain"
    "github.com/stretchr/testify/assert"
    "testing"
    "time"
)

func TestNewService(t *testing.T) {
    size := 10
    patterns := map[domain.Pair]domain.Pattern{
        "EURUSD": {Init: 1, Trend: -0.2, Roughness: 0.01},
        "USDRUB": {Init: 60, Trend: -2, Roughness: 0.03},
    }
    service := NewService(config.Generator{
        Pairs:        patterns,
        BufferSize:   size,
        UpdatePeriod: time.Microsecond,
    })

    service.mutex.RLock()
    assert.Equal(t, size, service.size)
    assert.Equal(t, size, service.Size())
    for name := range patterns {
        pair, ok := service.pairs[name]
        assert.True(t, ok)
        assert.NotNil(t, pair.engine)
    }
    service.mutex.RUnlock()

    time.Sleep(1 * time.Millisecond)

    service.mutex.RLock()
    for _, pair := range service.pairs {
        assert.Greater(t, 2*size, cap(pair.prices))
        assert.Equal(t, size, len(pair.prices))
    }
    service.mutex.RUnlock()
}

func TestBuffer_ReadAll(t *testing.T) {
    size := 10
    patterns := map[domain.Pair]domain.Pattern{
        "EURUSD": {Init: 1, Trend: -0.2, Roughness: 0.01},
        "USDRUB": {Init: 60, Trend: -2, Roughness: 0.03},
    }
    buffer := NewService(config.Generator{
        Pairs:        patterns,
        BufferSize:   size,
        UpdatePeriod: time.Microsecond,
    })

    time.Sleep(time.Millisecond)

    for name := range patterns {
        prices := make([]domain.Price, buffer.Size())
        ok, n := buffer.ReadAll(name, prices)
        assert.True(t, ok)
        assert.Equal(t, size, n)
        assert.NotContains(t, prices, domain.Price{})
    }
}
