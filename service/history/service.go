package history

import (
    "currencies-task/database/postgres"
    "currencies-task/domain"
    "currencies-task/rest/generator"
    "log"
    "time"
)

type (
    Service struct {
        database *postgres.Database
        pairs    []domain.Pair

        updatePeriod time.Duration
        client       *generator.Client
    }

    Config struct {
        Host         string
        Port         int
        UpdatePeriod time.Duration
    }
)

func NewService(client *generator.Client, database *postgres.Database, config Config) (*Service, error) {

    s := &Service{
        database:     database,
        updatePeriod: config.UpdatePeriod,
        client:       client,
    }

    go s.run()

    return s, nil
}

func (s *Service) PricesFromUntil(pair domain.Pair, from, until time.Time) ([]domain.Price, error) {
    dbPrices, err := s.database.PricesFromUntil(string(pair), from, until)
    if err != nil {
        return nil, err
    }

    prices := make([]domain.Price, 0, len(dbPrices))
    for _, dbPrice := range dbPrices {
        prices = append(prices, domain.Price{
            Value:      dbPrice.Value,
            UploadTime: dbPrice.UploadTime,
        })
    }
    return prices, nil
}

func (s *Service) run() {
    dbPairs, err := s.database.Pairs()
    if err != nil {
        log.Fatalln(err)
    }

    s.pairs = make([]domain.Pair, 0, len(dbPairs))
    for _, pair := range dbPairs {
        s.pairs = append(s.pairs, domain.Pair(pair.Name))
    }

    for range time.Tick(s.updatePeriod) {
        for _, pair := range s.pairs {
            if err = s.updatePair(pair); err != nil {
                log.Fatalln(err)
            }
        }
    }
}

func (s *Service) updatePair(pair domain.Pair) error {
    prices, err := s.client.GetPrices(pair)
    if err != nil {
        return err
    }

    dbPrices := make([]postgres.Price, 0, len(prices))
    for _, price := range prices {
        dbPrices = append(dbPrices, postgres.Price{
            Value:      price.Value,
            UploadTime: price.UploadTime,
        })
    }

    return s.database.SavePrices(string(pair), dbPrices)
}
